# Moysèés

Temos processo onde enviamos produtos para uma classificação manual. Alguns produtos são prioritários, outros não.  
Sua tarefa é criar uma aplicação onde seja possível definir quais produtos são prioritários. Tudo isso através de uma interface web.

Como _input_ você terá o _id_ do produto. A saída é uma marcação, no próprio banco de dados, indicando quais foram definidos como prioritário.

**Bônus 1.** Criar autenticação usuário/senha.  
**Bônus 2**. Supondo que o usuário queira marcar 1000 prioridades e o banco seja lento, crie uma alternativa para que o usuário não fique com a tela carregando por muito tempo.  
**Bônus 3.** Criar uma tela de status, onde o usuário poderá visualizar todos as prioridades que já enviou (de preferência com paginação).  
**Bônus 4.** Faça da aplicação um SPA (Single Page Application), utilizando AngularJS, Backbone ou qualquer framework que desejar.  

O _schema_ do banco de dados pode ser encontrado no arquivo `data.sql`.  
Repare que este é um _dump_ de um banco PostegreSQL, mas você pode trabalhar com o banco MySQL se preferir.

Fique a vontade para criar novas tabelas ou alterar a já existente caso você julgue necessário.

É obrigatório que a aplicação seja desenvolvida na linguagem Python. Você está livre para utilizar os frameworks/bibliotecas que julgar necessário  (tanto no front-end quanto no back-end).


## Como começar?
Faça um fork desse projeto base, ele já possui o _schema_ do banco de dados, deixando para você apenas a implementação do restante da aplicação.


## Critérios de Avaliação
Os seguintes aspectos do seu projeto serão avaliados, além de quão longe você prosseguiu no caminho da força:

* Agilidade.
* Legibilidade.
* Escopo.
* Organização do código.
* Existência e quantidade de bugs e gambiarras.
* Testes, testes, teste...

## Bônus Jedi
Colocar a aplicação para rodar em um ambiente onde possamos testar (Tanto no _Heroku_ quanto no _Google App Engine_ você pode fazer isso sem custos, mas pode escolher outra plataforma se preferir).

![Arco Iro](http://akaikki.com.br/wp-content/uploads/2012/12/moises.jpg)